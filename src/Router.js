import React, { lazy, Suspense } from "react";
import RouteWithLayout from "./utils/RouteWithLayout";
import { BrowserRouter, Switch, Route } from "react-router-dom";
import AppLayout from "./app/desktop/layouts/AppLayout";
import "./public/ststic/styles/index.css";
import "./public/ststic/styles/font.css";

const UsersStatistic = lazy(() => import("./app/desktop/pages/UsersStatistic"));

const Loading = () => {
  return <>loading</>;
};

function Router() {
  return (
    <BrowserRouter>
      <Switch>
        <Suspense fallback={<Loading />}>
          <RouteWithLayout
            exact
            path="/"
            component={UsersStatistic}
            layout={AppLayout}
          />
        </Suspense>
      </Switch>
    </BrowserRouter>
  );
}

export default Router;
