const configLinks = {
  API_URL: process.env.REACT_APP_API_URL,
};

export default configLinks;
