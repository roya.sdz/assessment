import styled from "styled-components";

const Styles = styled.div`
  .usersCountContainer {
    border-radius: 12px;
    background: #000000;
    color: #ffffff;
    width: fit-content;
  }

  .subTitle {
    color: gray;
  }

  .userCountIcon {
    width: 3rem;
    height: 3rem;
    background: #ffffff;
    border-radius: 10%;
  }

  .usersCount {
    border-left: solid gray;
    border-style: dotted;
    border-width: 0 0 0 2px;
  }

  .chartLable {
    color: #a90d80;
  }

  .chartLable::before {
    content: "";
    display: inline-block;
    width: 12px;
    height: 12px;
    background-color: #a90d80;
    margin: 0 5px 0 5px;
  }

  .chartContainer {
    flex: 1;
    min-height: 10rem;
  }

  @media only screen and (max-width: 576px) {
    .chartContainer {
      flex-direction: column;
      align-items: initial !important;
      margin-bottom: 2rem;
    }
  }
`;

export default Styles;
