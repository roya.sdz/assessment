import React, { useEffect, useState } from "react";
import Chart from "../../../components/chart/Chart";
import AppSelectBox from "../../../components/appSelectBox/AppSelectBox";
import Styles from "./styles/userChart";
import { Col, Row, Spinner } from "react-bootstrap";
import persianJs from "persianjs";

function UsersChart({ chartData, usersAgeRang, usersNumbercalculator }) {
  const [selectedAgeRange, setSelectedAgeRange] = useState({
    min: 0,
    max: 0,
    count: 0,
  });

  const [selectedChartData, setSelectedChartData] = useState(chartData);

  //for slice chart data
  const chartDataSlicer = (value, name) => {
    let updatedData = chartData.slice(
      selectedAgeRange.min,
      Number(selectedAgeRange.max) + 1
    );

    setSelectedChartData(updatedData);

    // set the total number of users of an interval
    if (updatedData.length > 0) {
      setSelectedAgeRange((prev) => {
        return {
          ...prev,
          count: usersNumbercalculator(updatedData),
        };
      });
    } else {
      setSelectedAgeRange((prev) => {
        return {
          ...prev,
          count: 0,
        };
      });
    }
  };

  const handleSelectChange = (value, name) => {
    setSelectedAgeRange((prev) => {
      return {
        ...prev,
        [name]: value,
      };
    });
  };

  const persianNumberGenerator = (number) => {
    if (number) {
      return persianJs(number).englishNumber().toString();
    } else {
      return "--";
    }
  };

  const getSelectItems = () => {
    let options = [];
    for (let i = usersAgeRang.min; i <= usersAgeRang.max; i++) {
      options.push(
        <option key={i} value={i}>
          {persianNumberGenerator(i)}
        </option>
      );
    }

    return options;
  };

  useEffect(() => {
    setSelectedChartData(chartData);
    setSelectedAgeRange({
      min: usersAgeRang.min,
      max: usersAgeRang.max,
      count: usersAgeRang.count,
    });
  }, [chartData]);

  useEffect(() => {
    if (selectedAgeRange.min && selectedAgeRange.max) {
      chartDataSlicer();
    }
  }, [selectedAgeRange.min, selectedAgeRange.max]);

  return (
    <Styles className="p-4 p-sm-5 d-flex flex-column min-vh-100">
      <div className="pb-3 border-bottom">
        <h2 className="app-font-bold desktop-font-1 pb-2">کاربران</h2>
        <p className="app-font-light desktop-font-3 subTitle">
          مشاهده تعداد کاربران{" "}
        </p>
      </div>

      <h2 className="pt-4 app-font-bold desktop-font-1">
        تعداد کاربران در بازه انتخابی
      </h2>
      <div className="usersCountContainer d-flex p-3 align-items-center mb-5 mt-5">
        <span className="userCountIcon" />
        <span className="font-Bold p-3 usersCount app-font-bold desktop-font-1">
          {persianNumberGenerator(selectedAgeRange.count)}
        </span>
        <div className="p-3 d-flex flex-column">
          <span className="app-font-bold desktop-font-2 pb-2">کاربران</span>
          <span className="app-font-light desktop-font-3">{`تعداد ${persianNumberGenerator(
            selectedAgeRange.count
          )} کاربر از سن ${persianNumberGenerator(
            selectedAgeRange.min
          )} تا ${persianNumberGenerator(selectedAgeRange.max)} سال`}</span>
        </div>
      </div>

      <h2 className="app-font-bold desktop-font-1">امار کاربران</h2>

      <div className="d-flex align-items-center justify-content-between chartContainer">
        <div className="d-flex align-items-baseline chartLable mt-4 mb-4">
          <div className="d-flex flex-column">
            <span className="app-font-bold desktop-font-1">
              {persianNumberGenerator(selectedAgeRange.count)}
            </span>
            <span lassName="app-font-light desktop-font-3">
              کاربر در بازه سنی مشخص شده
            </span>
          </div>
        </div>

        <Row>
          <Col>
            <AppSelectBox
              id="1"
              value={selectedAgeRange.min ? selectedAgeRange.min : ""}
              onChange={(e) => handleSelectChange(e.target.value, "min")}
              lable="از سال"
            >
              {getSelectItems().map((item, index) => item)}
            </AppSelectBox>
          </Col>
          <Col>
            <AppSelectBox
              id="2"
              value={selectedAgeRange.max ? selectedAgeRange.max : ""}
              onChange={(e) => handleSelectChange(e.target.value, "max")}
              lable="تا سال"
            >
              {getSelectItems().map((item, index) => item)}
            </AppSelectBox>
          </Col>
          <Col className="col-12"></Col>
        </Row>
      </div>
      <div className="chartContainer d-flex justify-content-center align-items-center">
        {selectedChartData ? (
          <Chart chartData={selectedChartData} />
        ) : (
          <Spinner animation="grow" variant="secondary" />
        )}
      </div>
    </Styles>
  );
}

export default UsersChart;
