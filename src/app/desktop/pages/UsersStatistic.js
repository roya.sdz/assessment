import React, { useEffect, useState } from "react";
import ApiUsers from "../../../core/apis/api.users";
import UsersChart from "../containers/usersStatistic/usersChart/UsersChart";

const UsersStatistic = (props) => {
  const [usersAgeRang, setUsersAgeRang] = useState({
    min: 0,
    max: 0,
    count: 0,
  });
  const [userChartData, setUserChartData] = useState(null);

  //generate random number for fetch users
  const randomIntegerCreator = (min, max) => {
    return Math.floor(Math.random() * (max - min + 1)) + min;
  };

  //for find minimum and maximudatam age of total users
  const getArrayMinMax = (array) => {
    const sortedArray = array.sort((firstElement, secondElement) => {
      return firstElement.dob.age - secondElement.dob.age;
    });
    return {
      min: sortedArray[0].dob.age,
      max: sortedArray[sortedArray.length - 1].dob.age,
    };
  };

  //for calculate the total number of users of an interval
  const usersNumbercalculator = (array) => {
    const total = array.reduce((previousValue, currentValue) => {
      return { count: previousValue.count + currentValue.count };
    });

    return total.count;
  };

  //create new array with users list for use in chart
  const formatData = (data) => {
    let chartData = [];
    const arrayRange = getArrayMinMax(data);

    //create array with length of maximum users age
    for (let i = 0; i <= arrayRange.max; i++) {
      chartData.push({
        age: i,
        count: 0,
      });
    }

    //calculate the number of age of each user
    for (let i = 0; i < data.length; i++) {
      let age = data[i].dob.age;
      chartData[age].count = chartData[age].count + 1;
    }

    //save data for use in chart container
    setUsersAgeRang({
      min: arrayRange.min,
      max: arrayRange.max,
      count: usersNumbercalculator(chartData),
    });

    return chartData;
  };

  const fetchUsers = async (usersCount) => {
    try {
      const res = await ApiUsers.getUsers(usersCount);
      if (res.status >= 200 && res.status <= 299) {
        const formatedData = formatData(res.data.results);
        setUserChartData(formatedData);
      }
    } catch (error) {
      alert(error);
    }
  };

  useEffect(() => {
    const usersCount = randomIntegerCreator(10, 100);
    console.log(usersCount);
    fetchUsers(usersCount);
  }, []);

  return (
    <div>
      <UsersChart
        chartData={userChartData}
        usersAgeRang={usersAgeRang}
        usersNumbercalculator={usersNumbercalculator}
      />
    </div>
  );
};

export default UsersStatistic;
