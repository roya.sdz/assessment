import React from "react";
import { Form } from "react-bootstrap";
import Styles from "./styles/appSelectBox";

const AppSelectBox = ({ children, value, onChange, id, lable }) => {
  return (
    <Styles>
      <Form.Select
        className="selectbox"
        key={id}
        aria-label={id}
        value={value}
        onChange={onChange}
      >
        <option disabled value="">
          {lable}
        </option>
        {children}
      </Form.Select>
    </Styles>
  );
};

export default AppSelectBox;
