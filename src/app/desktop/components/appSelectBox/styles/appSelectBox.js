import styled from "styled-components";

const Styles = styled.div`
  .selectbox {
    background-position: left 0.75rem center !important;
    padding: 0.375rem 0.75rem 0.375rem 2.25rem;
    border: 1px solid #e8eaec;
    box-shadow: 1px 2px 4px -1px #e8eaec;
  }
`;

export default Styles;
