import React from "react";
import { Bar } from "react-chartjs-2";
import persianJs from "persianjs";

const VerticalBar = ({ chartData }) => {
  const data = {
    labels: chartData
      .map((item) => item.age && persianJs(item.age).englishNumber().toString())
      .reverse(),
    datasets: [
      {
        data: chartData.map((item) => item.count).reverse(),
        backgroundColor: "#a90d80",
        borderWidth: 0,
      },
    ],
  };

  const options = {
    responsive: true,
    maintainAspectRatio: false,
    plugins: {
      tooltip: {
        enabled: false,
      },
      legend: {
        display: false,
      },
    },
    scales: {
      y: {
        grid: {
          borderDash: [2, 4],
        },
        position: "right",
        ticks: {
          callback: function (val) {
            return `${val && persianJs(val).englishNumber().toString()}`;
          },
        },
      },

      x: {
        grid: {
          borderDash: [2, 4],
        },
      },

      // yAxis: {
      //   // The axis for this scale is determined from the first letter of the id as `'x'`
      //   // It is recommended to specify `position` and / or `axis` explicitly.
      //   // type: "time",
      //   position: "right",
      // },
      // yAxes: [
      //   {
      //     position: "right",
      //     ticks: {
      //       beginAtZero: true,
      //     },
      //   },
      // ],
    },
  };
  return (
    <>
      <Bar data={data} options={options} />
    </>
  );
};

export default VerticalBar;
