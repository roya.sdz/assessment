import configLinks from "../../utils/configLinks";
import axios from "axios";

const ApiUsers = {
  getUsers: (usersCount) => {
    const url = `${configLinks.API_URL}/?results=${usersCount}`;
    return axios.get(url);
  },
};

export default ApiUsers;
